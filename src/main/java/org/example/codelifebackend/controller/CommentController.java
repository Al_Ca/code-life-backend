package org.example.codelifebackend.controller;

import org.apache.ibatis.jdbc.Null;
import org.example.codelifebackend.pojo.Article;
import org.example.codelifebackend.pojo.Comment;
import org.example.codelifebackend.pojo.R;
import org.example.codelifebackend.service.CommentService;
import org.example.codelifebackend.service.UserService;
import org.example.codelifebackend.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class CommentController {
    @Autowired
    private CommentService commentService;
    @Autowired
    private UserService userService;
    @Autowired
    private ArticleService articleService;
    @GetMapping("/api/comment/show")
    @CrossOrigin(origins = "http://localhost:8080")
    public R showComment(@RequestBody Map<String, String> re_map) {
        List<Comment> res = commentService.getComment();
        if (res == null) return R.error("获取失败");
        return R.success(res);
    }

    @PostMapping("/api/comment/add")
    @CrossOrigin(origins = "http://localhost:8080")
    public R addComment(@RequestBody Map<String, String> re_map) {
        String content = re_map.get("content");
        int userId = Integer.parseInt(re_map.get("author_id"));
        int articleId = Integer.parseInt(re_map.get("article_id"));
 //需要加上文章评论
        String name = null;
        if(userService.findUserById(userId).getName()==null){
            name = "无名";
        }else{
            name=userService.findUserById(userId).getName();
        }
        Comment comment = new Comment(userId, articleId, content, name);
        Comment res = commentService.addComment(comment);
        if (res == null) return R.error("发布失败");
        Article article=articleService.getArticle(articleId);
        article.setComment(article.getComment()+1);
        articleService.updateArticle(article);
        return R.success("发布成功");
    }
}