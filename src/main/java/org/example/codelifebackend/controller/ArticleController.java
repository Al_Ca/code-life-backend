package org.example.codelifebackend.controller;

import org.example.codelifebackend.pojo.Article;
import org.example.codelifebackend.pojo.R;
import org.example.codelifebackend.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:8080")
@RequestMapping("/api/blog")
public class ArticleController {
    @Autowired
    private ArticleService articleService;

    @GetMapping("/getArticle")
    @CrossOrigin(origins = "http://localhost:8080")
    public R<Article> getArticle(@RequestParam int id){
        Article res = articleService.getArticle(id);
        if(res==null)return R.error("获取失败");
        return R.success(res);
    }

    @GetMapping("/getPopular")
    @CrossOrigin(origins = "http://localhost:8080")
    public R<List<Article>> getTopArticles(){
        List<Article> res = articleService.getTopArticle();
        if(res==null)return R.error("获取失败");
        return R.success(res);
    }

    @DeleteMapping("/delete/{id}")
    @CrossOrigin(origins = "http://localhost:8080")
    public R<Integer> deleteArticle(@PathVariable int id){
        if(articleService.deleteArticle(id))return R.success(id);
        return R.error("删除失败");
    }

    @PostMapping("/upload")
    public R<Article> addArticle(@RequestBody Article article){
        Article res = articleService.addArticle(article);
        if(res==null)return R.error("添加失败");
        return R.success(res);
    }

}
