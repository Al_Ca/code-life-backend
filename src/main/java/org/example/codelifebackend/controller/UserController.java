package org.example.codelifebackend.controller;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.ibatis.annotations.Update;
import org.example.codelifebackend.pojo.Article;
import org.example.codelifebackend.pojo.R;
import org.example.codelifebackend.pojo.User;
import org.example.codelifebackend.service.ArticleService;
import org.example.codelifebackend.service.UserService;
import org.example.codelifebackend.Utils.JwtUtil;
import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.cloud.openfeign.FeignClient;
//import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
@RestController
public class UserController {
    @Autowired
    private UserService userService;
    @Autowired
    private ArticleService articleService;
    @Autowired
    private EmailController emailController;
    private JwtUtil jwtUtil = new JwtUtil();
    @GetMapping("/user/findUser/id")
    @CrossOrigin(origins = "http://localhost:8080")
    public R<Object> findUser(@RequestParam int id) {
     User user=  userService.findUserById(id);
        List<Article> article=articleService.getArticleByAuthor(id);
        int commentNum = articleService.getCommentCount(id);
        int likeNum = articleService.getLikeCounty(id);
        Map<String, Object> data = new HashMap<>();
        data.put("user", user);
        data.put("article", article);
        data.put("commentNum",commentNum);
        data.put("likeNum",likeNum);
        return R.success(data);
    }
    @GetMapping("/api/blog/getAuthor")
    @CrossOrigin(origins = "http://localhost:8080")
    public R<Object> findAuthur(@RequestParam int id) {
        User user=   userService.findUserById(id);
        int commentNum = articleService.getCommentCount(id);
        int likeNum = articleService.getLikeCounty(id);
        int puliNum =articleService.getPublicationCount(id);
        Map<String, Object> data = new HashMap<>();
        data.put("author_name", user.getName());
        data.put("author_publication_count", puliNum);
        data.put("author_comment_count",commentNum);
        data.put("author_like_count",likeNum);
        return R.success(data);
    }
    @GetMapping("/user/findUser/email")
    @CrossOrigin(origins = "http://localhost:8080")
    public R<User> findUser(@RequestParam String emial) {
        return R.success(userService.findUserByEmail(emial));
    }
    @PostMapping("/user/register")
    @CrossOrigin(origins = "http://localhost:8080")
    public R<Object> register(@RequestBody Map<String,String> re_map) {
        String email = re_map.get("email");
        int code = Integer.parseInt(re_map.get("code"));
//        String token=null;
        if(emailController.emailCheck(email,code)) {
                if (userService.findUserByEmail(email) == null) {
                        User userSet = new User(email);
                        userService.registerNewUser(userSet);
//                 token = jwtUtil.createToken(userService.findUserByEmail(email));
                }
            Map<String, Object> data = new HashMap<>();
            data.put("user",userService.findUserByEmail(email));
            data.put("id",userService.findUserByEmail(email).getId());
            return R.success(data);
//            return R.success(simpleUser);

        }
        return R.error("register false");
    }

    @PostMapping("/user/update/info")
    @CrossOrigin(origins = "http://localhost:8080")
    public R<String> updateInfo(@RequestBody Map<String,String> re_map){
        String name=re_map.get("newNick");
        int gender = Integer.parseInt(re_map.get("newGender"));
        int id = Integer.parseInt(re_map.get("id"));
        String intro = re_map.get("newInto");
        String edu = re_map.get("newEdu");
        if(userService.findUserById(id)==null){
            return R.error("wrong user");}
       userService.updateUser(id,gender,name,intro,edu);
            return R.success("ok");

    }
    @PostMapping("/user/update/password")
    @CrossOrigin(origins = "http://localhost:8080")
    public R<String> updatePass(@RequestBody Map<String,String> re_map){
        int id = Integer.parseInt(re_map.get("id"));
        String oldPass = re_map.get("oldPwd");
        String newPass = re_map.get("newPwd");
        if(!passwordCheck(newPass)){
            return  R.error("disable password");
        }
        if( userService.updatePass(id,oldPass,newPass)!=null){
            return R.success("ok");
        }else{
            return  R.error("wrong");
        }
    }

    @PostMapping("/user/update/email")
    @CrossOrigin(origins = "http://localhost:8080")
    public R<String> updateEmail(@RequestBody Map<String,String> re_map){
        int id = Integer.parseInt(re_map.get("id"));
        int code = Integer.parseInt(re_map.get("capN"));
        String newEmail = re_map.get("email");
//        String token=null;
        if(emailController.emailCheck(newEmail,code)) {
            if (userService.findUserByEmail(newEmail) == null) {
                userService.updateEmail(id,newEmail);
                return R.success("ok");

            }
        }
        return R.error("register false");
    }

//    @PostMapping("/user/changePassword")
//    public R<String> changePassword(@RequestBody Map<String,String> re_map) {
//        String password = re_map.get("password");
//        String email = re_map.get("email");
//        int code = Integer.parseInt(re_map.get("code"));
//        if(emailController.emailCheck(email,code)) {
//            userService.changePassword(email,password);
//            User tempUser= userService.findUserByEmail(email);
//            String json = null;
//            try {
//                json = mapper.writeValueAsString(tempUser);
//                stringRedisTemplate.opsForValue().set(tempUser.getEmail(),json);
//            } catch (JsonProcessingException e) {
//                throw new RuntimeException(e);
//            }
//            return R.success("change success");
//        }
//        return R.error("change false");
//    }
//    @PostMapping("/user/changeUsername")
//    public R<String> changeUsername(@RequestBody Map<String,String> re_map) {
//        String username = re_map.get("username");
//        System.out.println(username);
//        String email = re_map.get("email");
//        userService.changeUsername(email,username);
//        User tempUser= userService.findUserByEmail(email);
//        String json = null;
//        try {
//            json = mapper.writeValueAsString(tempUser);
//            stringRedisTemplate.opsForValue().set(tempUser.getEmail(),json);
//        } catch (JsonProcessingException e) {
//            throw new RuntimeException(e);
//        }
//        return R.success("change success");
//    }

    @GetMapping("/user/login")
    @CrossOrigin(origins = "http://localhost:8080")
    public R<Object> login(@RequestParam String email,String password) {
        if(passwordCheck(password)){
            if(userService.findUserByEmail(email).getPassword().equals(password))
            {
                Map<String, Object> data = new HashMap<>();
                data.put("user",userService.findUserByEmail(email));
                data.put("id",userService.findUserByEmail(email).getId());
//                String token = jwtUtil.createToken(userService.findUserByEmail(email));
                return R.success(data);
            }
        }
        return R.error("login false");
    }



    private boolean passwordCheck(String password)
    {
        if (password.length() >=17) {
            return false;
        }
        for (char c : password.toCharArray()) {
            if (!Character.isLetterOrDigit(c) && c != '_') {
                return false;
            }
        }
        return true;
    }
}
