package org.example.codelifebackend.controller;

import org.example.codelifebackend.pojo.R;
import org.example.codelifebackend.pojo.Email;
import org.example.codelifebackend.service.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Map;
import java.util.Random;

@RestController
public class EmailController {
    @Autowired
    private JavaMailSender mailSender;
    @Value("${spring.mail.username}")
    private String sender;
    @Autowired
    private EmailService emailService;
    @PostMapping("/user/emailSend")
    @CrossOrigin(origins = "http://localhost:8080")
    public R<String> emailSend(@RequestBody Map<String,String> re_map)
    {
        String email = re_map.get("email");
        Random random=new Random();
        int randomNumber = random.nextInt(900000) + 100000;
        LocalDateTime nowTime = LocalDateTime.now();
        Email tempEmail = new Email(email,randomNumber,nowTime);
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("SciNet" + '<' + sender + '>');
        message.setTo(email);
        message.setSubject("欢迎访问SciNet");
        String content = "【验证码】您的验证码为：" + randomNumber + " 。 验证码五分钟内有效，逾期作废。\n\n";
        message.setText(content);
        mailSender.send(message);
        if(emailService.findEmail(tempEmail.getEmail())!=null)
        {
            emailService.updateEmail(tempEmail);
        }
        else {
            emailService.sendNewEmail(tempEmail);
        }
        return R.success(email);
    }

    public boolean emailCheck(String email,int code)
    {
        LocalDateTime nowTime = LocalDateTime.now();
        Email tempEmail = new Email(email,code,nowTime);
        Email backEmail = emailService.findEmail(tempEmail.getEmail());
        if(backEmail!=null)
        {
            Duration duration = Duration.between(tempEmail.getSendTime(), backEmail.getSendTime());
            long minutes = duration.toMinutes();
            if(backEmail.getCode()==code&&minutes<=5)
            {
                return true;
            }
        }
        return false;
    }
}
