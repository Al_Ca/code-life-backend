package org.example.codelifebackend.service;

import org.example.codelifebackend.pojo.Email;
import org.example.codelifebackend.dao.EmailDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class EmailService {
    @Autowired
    private EmailDao emailDao;
    public void sendNewEmail(Email email){emailDao.sendNewEmail(email);}
    public Email findEmail(String email){return emailDao.findEmail(email);}
    public void updateEmail(Email email){emailDao.updateEmail(email);}
}
