package org.example.codelifebackend.service;

import org.example.codelifebackend.dao.CommentDao;
import org.example.codelifebackend.pojo.Comment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CommentService {
    @Autowired
    private CommentDao commentDao;
    public  List<Comment> getComment() {
        try {
            return commentDao.getComment();
        } catch (Exception e){
            return null;
        }
    }
    public  Comment addComment(Comment comment){
           try {
         commentDao.addComment(comment);
    } catch (Exception e){
        return null;
    }
           return comment;
    }
}
