package org.example.codelifebackend.service;

import org.example.codelifebackend.dao.UserDao;
import org.example.codelifebackend.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserDao userDao;
    public User findUserByEmail(String email)
    {
        return userDao.getByEmail(email);
    }
    public void registerNewUser(User user){userDao.addUser(user);}

    public void updateUser(int id ,int gender,String name,String intro,String edu) {
 userDao.updateUser(id,gender,name,intro,edu);
    }
    public void updateEmail(int id ,String email) {
        userDao.updateEmail(id,email);
    }
    public User  updatePass(int id,String oldPass,String newPass){
        if(oldPass==null){
            userDao.updatePass(id,newPass);
            return  userDao.getById(id);
        }
        else if(userDao.getById(id).getEmail().equals(oldPass)){
            userDao.updatePass(id,newPass);
            return  userDao.getById(id);
        }
        else return null;
    }
    public User findUserById(int id){return userDao.getById(id);};

}
