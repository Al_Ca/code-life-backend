package org.example.codelifebackend.service;

import org.example.codelifebackend.dao.ArticleDao;
import org.example.codelifebackend.pojo.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ArticleService {
    @Autowired
    private ArticleDao articleDao;

    public Article getArticle(int id){
        try {
            return articleDao.getArticleById(id);
        } catch (Exception e){
            return null;
        }
    }

    public List<Article> getArticleByAuthor(int id){
        try{
            return articleDao.getArticleByAuthor(id);
        } catch (Exception e){
            return null;
        }
    }

    public Integer getLikeCounty(int id){
        try{
            List<Article> res = articleDao.getArticleByAuthor(id);
            int ans=0;
            for(Article article : res){
                ans+=article.getLike();
            }
            return ans;
        } catch (Exception e){
            return null;
        }
    }
        public Integer getPublicationCount(int id){
        try{
            List<Article> res = articleDao.getArticleByAuthor(id);
            return res.size();
        } catch (Exception e){
            return null;
        }
    }
    public Integer getCommentCount(int id){
        try{
            List<Article> res = articleDao.getArticleByAuthor(id);
            int ans=0;
            for(Article article : res){
                ans+=article.getComment();
            }
            return ans;
        } catch (Exception e){
            return null;
        }
    }

    public List<Article> getTopArticle(){
        try {
            return articleDao.getTopArticles();
        } catch (Exception e){
            return null;
        }
    }

    public boolean deleteArticle(int id){
        try {
            articleDao.deleteArticle(id);
        } catch (Exception e){
            return false;
        }
        return true;
    }

    public Article addArticle(Article article){
        try {
            articleDao.addArticle(article);
        } catch (Exception e){
            return null;
        }
        return article;
    }

    public boolean updateArticle(Article article){
        try{
            articleDao.updateArticle(article);
        } catch (Exception e){
            return false;
        }
        return true;
    }
}
