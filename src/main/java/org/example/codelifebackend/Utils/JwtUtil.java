package org.example.codelifebackend.Utils;
import com.alibaba.fastjson2.JSON;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.stereotype.Component;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import java.util.*;


@Component
public class JwtUtil {
    private static final long JWT_EXPIRE =4 * 30 * 60 * 1000L;
    private static final String JWT_KEY = "123456";

    public String createToken(Object data) {
        long currentTime = System.currentTimeMillis();
        long expTime = currentTime + JWT_EXPIRE;
        JwtBuilder builder = Jwts.builder()
                .setId(UUID.randomUUID() + "")
                .setSubject(JSON.toJSONString(data))
                .setIssuer("system")
                .setIssuedAt(new Date(currentTime))
                .signWith(SignatureAlgorithm.HS256, encodeSecret())
                .setExpiration(new Date(expTime));
        return builder.compact();
    }

    private SecretKey encodeSecret() {
        byte[] encode = Base64.getEncoder().encode(JwtUtil.JWT_KEY.getBytes());
        return new SecretKeySpec(encode, 0, encode.length, "AES");
    }

    public boolean isSimpleUser(Claims claims) {
        return claims.getSubject().contains("userId");
    }

    public Claims parseToken(String token) {
        Claims body = Jwts.parser()
                .setSigningKey(encodeSecret())
                .parseClaimsJws(token)
                .getBody();
        long created = body.getIssuedAt().getTime();
        long expired = created + JWT_EXPIRE;
        long currentTime = System.currentTimeMillis();
        if (currentTime < expired) {
            return body;
        }
        return null;
    }
}
