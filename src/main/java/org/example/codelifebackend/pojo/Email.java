package org.example.codelifebackend.pojo;

import java.time.LocalDateTime;


public class Email {
    private int id;
    private String email;
    private int code;
    private LocalDateTime sendTime;
    public Email(){}
    public Email(String email, int code, LocalDateTime sendTime)
    {
        this.email=email;
        this.sendTime=sendTime;
        this.code=code;
    }
    public int getCode()
    {
        return this.code;
    }
    public LocalDateTime getSendTime()
    {
        return this.sendTime;
    }
    public String getEmail()
    {
        return this.email;
    }
}
