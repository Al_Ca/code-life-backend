package org.example.codelifebackend.pojo;

import java.time.LocalDateTime;

public class Article {
    private Integer id;
    private String title;//标题
    private Integer author;//作者
    private String content;//文章
    private String abst;//摘要
    private String tag;//标签
    private Integer like;//点赞
    private Integer unlike;//踩
    private Integer comment;//评论
    private LocalDateTime time;//时间

    public Article() {
    }

    public Article(String title, Integer author, String content, String abst, String tag, Integer like, Integer unlike, Integer comment, LocalDateTime time) {
        this.title = title;
        this.author = author;
        this.content = content;
        this.abst = abst;
        this.tag = tag;
        this.like = like;
        this.unlike = unlike;
        this.comment = comment;
        this.time = time;
    }

    public Article(Integer id, String title, Integer author, String content, String abst, String tag, Integer like, Integer unlike, Integer comment, LocalDateTime time) {
        this.id = id;
        this.title = title;
        this.author = author;
        this.content = content;
        this.abst = abst;
        this.tag = tag;
        this.like = like;
        this.unlike = unlike;
        this.comment = comment;
        this.time = time;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUnlike() {
        return unlike;
    }

    public void setUnlike(Integer unlike) {
        this.unlike = unlike;
    }

    public Article(String title, Integer author, String content, String abst, String tag, Integer like, Integer comment, LocalDateTime time) {
        this.title = title;
        this.author = author;
        this.content = content;
        this.abst = abst;
        this.tag = tag;
        this.like = like;
        this.comment = comment;
        this.time = time;
    }

    public Integer getAuthor() {
        return author;
    }

    public void setAuthor(Integer author) {
        this.author = author;
    }

    public LocalDateTime getTime() {
        return time;
    }

    public void setTime(LocalDateTime time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getAbst() {
        return abst;
    }

    public void setAbst(String abst) {
        this.abst = abst;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Integer getLike() {
        return like;
    }

    public void setLike(Integer like) {
        this.like = like;
    }

    public Integer getComment() {
        return comment;
    }

    public void setComment(Integer comment) {
        this.comment = comment;
    }
}
