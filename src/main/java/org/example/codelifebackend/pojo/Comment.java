package org.example.codelifebackend.pojo;


import java.time.LocalDateTime;

public class Comment {

    private int id;
    private int userId;
    private int articleId;

    private String name;
    private LocalDateTime timestamp;
    private String content;//简介
    public Comment(){}

    public Comment (int userId,int articleId,String content,String name){
    this.userId=userId;
    this.articleId=articleId;
    this.content=content;
    this.name =name;
    this.timestamp = LocalDateTime.now();
    }

}
