package org.example.codelifebackend.pojo;

import java.time.LocalDateTime;


public class User {

    private int id;

    private String name;

    private String email;

    private int gender;

    private String intro;//简介

    private String password;

    private String edu;//学历
public User(){}

    public User (String email){
        this.email=email;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getGender() {
        return gender;
    }
    public void setGender(int gender) {
        this.gender = gender;
    }
    public String getEmail() {
        return email;
    }
    public int getId(){
    return id;
    }
    public String getEdu() {
        return edu;
    }
    public void setEdu(String edu) {
        this.edu = edu;
    }
    public String getIntro() {
        return intro;
    }
    public void setIntro(String intro) {
        this.intro = intro;
    }
}
