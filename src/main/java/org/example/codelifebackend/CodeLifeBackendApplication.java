package org.example.codelifebackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
public class CodeLifeBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(CodeLifeBackendApplication.class, args);
	}

}
