package org.example.codelifebackend.dao;


import org.apache.ibatis.annotations.*;
import org.example.codelifebackend.pojo.Article;
import org.example.codelifebackend.pojo.User;

import java.util.List;

@Mapper
public interface UserDao {
    @Select("SELECT * FROM user WHERE email = #{email}")
    public  User getByEmail(String email);
    @Select("SELECT * FROM user WHERE id = #{id}")
    public  User getById(int id);

    @Select("SELECT * FROM User")
    public List<Article> getAllUser();

    @Insert("INSERT INTO user (email) " +
            "VALUES (#{email})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    public void addUser(User user);

    @Update("UPDATE user SET name=#{name},gender=#{gender},intro=#{intro},`edu`=#{edu} WHERE `id`=#{id}")
    public void updateUser(int id,int gender,String name,String intro,String edu);

    @Update("UPDATE user SET password=#{password} WHERE `id`=#{id}")
    public void updatePass(int id,String password);
    @Update("UPDATE user SET email=#{email} WHERE `id`=#{id}")
    public void updateEmail(int id,String email);
}
