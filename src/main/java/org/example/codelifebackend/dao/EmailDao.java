package org.example.codelifebackend.dao;

import org.apache.ibatis.annotations.*;
import org.example.codelifebackend.pojo.Email;
import org.example.codelifebackend.pojo.User;
import org.springframework.stereotype.Repository;

@Mapper
public interface EmailDao {
    @Insert("INSERT INTO emailInfo (email,sendTime,code) " +
            "VALUES (#{email},#{sendTime},#{code})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    void sendNewEmail(Email email);
    @Select("SELECT * FROM emailInfo WHERE `email` = #{email}")
    public Email findEmail(String email);
    @Update("UPDATE emailInfo SET code=#{code},sendTime=#{sendTime} WHERE `email`=#{email}")
    void updateEmail(Email email);
}
