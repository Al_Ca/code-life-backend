package org.example.codelifebackend.dao;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;
import org.example.codelifebackend.pojo.Comment;

import java.util.List;
@Mapper
public interface CommentDao {
    @Select("SELECT * FROM user WHERE `email` = #{email}")
    List<Comment> getComment();

    @Insert("INSERT INTO comment (content,userId,articleId,time,name) " +
            "VALUES (#{content},#{userId},#{articleId},#{timestamp},#{name})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    void addComment(Comment comment);
}
