package org.example.codelifebackend.dao;
import org.apache.ibatis.annotations.*;
import org.example.codelifebackend.pojo.Article;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface ArticleDao {
    @Select("SELECT * FROM article")
    public List<Article> getAllArticle();

    @Select("SELECT * FROM article WHERE `id`=#{id}")
    public Article getArticleById(int id);

    @Select("SELECT * FROM article ORDER BY `like` DESC LIMIT 10")
    public List<Article> getTopArticles();

    @Select("SELECT * FROM article WHERE author=#{id}")
    public List<Article> getArticleByAuthor(int id);

    @Insert("INSERT INTO article (title,author,content,abst,tag,`like`,unlike,`comment`,time) " +
            "VALUES (#{title},#{author},#{content},#{abst},#{tag},#{like},#{unlike},#{comment},#{time})")
    @Options(useGeneratedKeys = true,keyProperty = "id")
    public void addArticle(Article article);

    @Update("UPDATE article SET title=#{title},author=#{author},content=#{content},abst=#{abst},tag=#{tag},`like`=#{like}," +
            "unlike=#{unlike},`comment`=#{comment},time=#{time} WHERE `id`=#{id}")
    public void updateArticle(Article article);

    @Delete("DELETE FROM article WHERE `id` = #{id}")
    public void deleteArticle(int id);
}
